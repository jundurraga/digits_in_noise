from PyQt5 import QtWidgets
import sys
from control.experiment import AdaptiveTrackingExperiment
from datetime import datetime
import sounddevice as sd
__author__ = 'jaime undurraga'

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle(QtWidgets.QStyleFactory.create('plastique'))
    app.setStyleSheet('QWidget{border: 1px solid gray; background-color: black; color: white}')
    ex = AdaptiveTrackingExperiment('digits_in_noise', experiment_parameters={'speaker': 'female_0'})

    """Setup adaptive tracking parameters"""
    ex.adaptive_tracking_parameters.tracked_parameter = 'snr'
    ex.adaptive_tracking_parameters.start_values = [10]
    ex.adaptive_tracking_parameters.step_sizes = [-6, -3, -2]
    ex.adaptive_tracking_parameters.back_step_sizes = [6, 3, 2]
    ex.adaptive_tracking_parameters.n_reversals_per_step = [2, 2, 6]
    ex.adaptive_tracking_parameters.n_correct_down = 2
    ex.adaptive_tracking_parameters.n_incorrect_up = 1

    """participant information, date, and ITD conditions to be used are set are setup here"""
    ex.participant_id = 'LVY'
    ex.participant_dob = datetime.strptime('Jun/9/1993', '%b/%d/%Y')
    ex.test_values = ['inverse', 0, -0.0001, -0.0002, -0.0004, -0.0008]

    """The following values are setup for the specific soundcard and transducers. They define the relative RMS that will
    result on a given SPL as calibrated with a sound level meter. This need to be determined in the lab and kept fixed. 
    Once calibrated, do not touch these two values below as are used as the reference to set you at a desired level"""
    reference_spl = 65
    reference_rms = 0.021

    """input the desired level that you want to use"""
    desired_spl = 65

    ex.reference_spl = desired_spl
    ex.reference_rms = reference_rms * 10 ** ((desired_spl - reference_spl)/20)
    ex.randomize = True

    # show available devices.
    print("Available devices (id number, name, hostapi, in/out channels): \n {:}".format(sd.query_devices()))

    """to use the default soundcard uncomment next line"""
    ex.sound_device = None

    """to use ASIO drivers, assuming you have a compatible soundcard you can set it up as shown below. Note 
    that channel numbers start at zero"""
    ex.sound_device = 'ASIO Fireface USB'
    ex.sound_device_channels = [2, 3]

    """To use windows, mac or linux devices with specific channel mapping see example below.
    Note that channel numbers start at 1. Please note that in linux RME sound cards are exposed properly via 
    jack audio. Thus make sure you have jack running with RME sound card and select the jack audio connection
    matching the RME device, e.g. system, JACK Audio Connection Kit (18 in, 18 out) for RME UCX.  
    """
    # ex.sound_device = 13
    # ex.sound_device_channels = [3, 4]

    ex.begin()
    sys.exit(app.exec_())

