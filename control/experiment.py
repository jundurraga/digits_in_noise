from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget
import random
from control.definitions import DigitsInNoiseRunner, SNRMode, AdaptiveTrackingParameters
from datetime import datetime

__author__ = 'jaime undurraga'


class AdaptiveTrackingExperiment(QWidget):
    def __init__(self, experiment_type: str = 'digits_in_noise',
                 experiment_parameters={}):
        super(AdaptiveTrackingExperiment, self).__init__()
        self.test_values = [0.0001, 0.0002]
        self.randomize = True
        self._value_index = 0
        self.reference_rms = 0.005
        self.reference_spl = 62.0
        self.runner = None
        self._test_values = self.test_values
        self.participant_id = ''
        self.participant_dob: datetime = None
        self.sound_device = 44
        self.sound_device_channels = [1, 2]
        self.experiment_type = experiment_type
        self.experiment_parameters = experiment_parameters
        self.adaptive_tracking_parameters: AdaptiveTrackingParameters = AdaptiveTrackingParameters()

    def begin(self):
        if self.experiment_type == 'digits_in_noise':
            self.runner = DigitsInNoiseRunner(self, **self.experiment_parameters)
        self.runner.signal_runner_start.connect(self.run_experiment)

    @pyqtSlot()
    def run_experiment(self):
        if not self.prepare_run():
            self.runner.play()
            self._value_index += 1
        else:
            self.close()

    @pyqtSlot()
    def prepare_run(self):
        finish = False
        if self._value_index == 0:
            self._test_values = self.test_values
            if self.randomize:
                self._test_values = random.sample(self.test_values, len(self.test_values))
        if self._value_index == len(self._test_values):
            finish = True

        if not finish:
            test_value = self._test_values[self._value_index]
            # if last run, change message
            if self._value_index == len(self._test_values) - 1:
                new_end_message = '{:} / {:} Click to Close'.format(self._value_index + 1, len(self._test_values))
            else:
                new_end_message = '{:} / {:} Click to Continue'.format(self._value_index + 1, len(self._test_values))
            self.runner.initialize('', new_end_message, self.adaptive_tracking_parameters)
            self.runner.adaptive_tracker.participant_id = self.participant_id
            self.runner.adaptive_tracker.participant_dob = self.participant_dob
            self.runner.reference_spl = self.reference_spl
            self.runner.number_of_digits = 3
            self.runner.sound_device = self.sound_device
            self.runner.sound_device_channels = self.sound_device_channels

            noise_parameters = {'reference_rms': self.reference_rms,
                                'duration': 4.0}
            signal_parameters = {'itd': test_value, 'snr_mode': SNRMode.fix_masker}
            self.runner.adaptive_tracker.extra_parameters = dict(noise_parameters, **signal_parameters,
                                                                 **self.experiment_parameters)
            self.runner.noise_parameters = noise_parameters
            self.runner.signal_parameters = signal_parameters
        return finish
