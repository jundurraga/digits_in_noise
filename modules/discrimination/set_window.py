from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal


class CustomButton(QtWidgets.QPushButton):
    custom_clicked = QtCore.pyqtSignal()

    def mousePressEvent(self, event):
        self.custom_clicked.emit()


class DigitsInNoiseWindow(QWidget):
    signal_response = pyqtSignal(str, name='signal_response')
    signal_start = pyqtSignal(name='signal_start')

    def __init__(self, parent):
        super(DigitsInNoiseWindow, self).__init__(parent)
        self.dialog = QtWidgets.QDialog(self)
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Digits in Noise")

        main_frame = QtWidgets.QFrame()
        main_layout = QtWidgets.QVBoxLayout(main_frame)
        _frame = self.__set_digits_in_noise_frame()
        main_layout.addWidget(_frame)
        self.dialog.setLayout(main_layout)
        self.dialog.showFullScreen()

    def __set_digits_in_noise_tab(self):
        main_frame = QtWidgets.QFrame()
        main_lay_out = QtWidgets.QVBoxLayout(main_frame)
        tab = QWidget()
        frame = self.__set_digits_in_noise_options()
        main_lay_out.addWidget(frame)
        tab.setLayout(main_lay_out)
        tab.setObjectName('tab_digits in noise')
        return tab

    @staticmethod
    def __set_digits_in_noise_frame():
        frame = QtWidgets.QFrame()
        options_box = QtWidgets.QGridLayout(frame)
        label_message = QtWidgets.QLabel('Type the numbers that you hear')
        label_message.setStyleSheet("font-size:32px")
        label_message.setObjectName('label_message')
        label_message.setAlignment(QtCore.Qt.AlignCenter)
        options_box.addWidget(label_message, *[0, 0])

        qline_edit_answer = QtWidgets.QLineEdit('')
        qline_edit_answer.setObjectName('q_line_edit_answer')
        qline_edit_answer.setStyleSheet("font-size:32px")
        qline_edit_answer.setAlignment(QtCore.Qt.AlignCenter)
        options_box.addWidget(qline_edit_answer, *[1, 0])

        qbutton_start = CustomButton('Start')
        qbutton_start.setObjectName('q_button_start')
        qbutton_start.setStyleSheet("font-size:32px")
        options_box.addWidget(qbutton_start, *[2, 0])

        return frame
